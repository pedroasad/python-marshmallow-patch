=============
API reference
=============

:mod:`marshmallow_patch.config`
===============================

.. automodule:: marshmallow_patch.config
    :members:

:mod:`marshmallow_patch.marshmallow`
====================================

.. automodule:: marshmallow_patch.marshmallow
    :members:

:mod:`marshmallow_patch.marshmallow.fields`
===========================================

.. automodule:: marshmallow_patch.marshmallow.fields
    :special-members: units
    :members: oneOf, Nested, NumpyArray, PhysicalQuantity, PhysicalUnit

