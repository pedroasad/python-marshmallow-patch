import attr
import numpy
import pytest

from itertools import product
from typing import Callable
from marshmallow_patch.config import units
from marshmallow_patch.marshmallow import ValidationError, fields

Quantity = units.Quantity
Unit = units.Unit


class Test_NumpyArray:
    """Test :class:`v2.server.rest.fields.NumpyArray` field specifiers. Test cases use the :meth:`serialize` and
    :meth:`deserialize` callable fixtures, which are shortcuts for using the actual :class:`marshmallow.fields.Field`
    (de)serialization API. The :meth:`an_array` fixture returns different arrays declared in the :attr:`tested_arrays`
    dictionary for testing.
    """

    @attr.s
    class Fixture:
        numpy_array: numpy.ndarray = attr.ib()
        serialized: dict = attr.ib()

    tested_arrays = {
        "plain-array": Fixture(
            numpy_array=numpy.array(
                [[1, 2, 3], [4, 5, 6]], dtype="i8"  # A plain (scalar) array.
            ),
            serialized={
                "dtype": str(numpy.dtype("i8")),
                "shape": (2, 3),
                "array": [[1, 2, 3], [4, 5, 6]],
                "fields": None,
            },
        ),
        "record-array": Fixture(
            numpy_array=numpy.array(
                [  # A record array with plain (scalar) fields.
                    (1, 2.0, "3"),
                    (4, 5.0, "6"),
                ],
                dtype=[("a", "i8"), ("b", "f4"), ("c", "U5")],
            ),
            serialized={
                "dtype": None,
                "shape": (2,),
                "array": None,
                "fields": {
                    "a": {
                        "dtype": str(numpy.dtype("i8")),
                        "shape": (2,),
                        "array": [1, 4],
                        "fields": None,
                    },
                    "b": {
                        "dtype": str(numpy.dtype("f4")),
                        "shape": (2,),
                        "array": [2.0, 5.0],
                        "fields": None,
                    },
                    "c": {
                        "dtype": str(numpy.dtype("U5")),
                        "shape": (2,),
                        "array": ["3", "6"],
                        "fields": None,
                    },
                },
            },
        ),
        "record-nested-array": Fixture(
            numpy_array=numpy.array(
                [  # A record array with plain (scalar) fields.
                    (1, numpy.array([[2.0, 4.0], [3.0, 9.0], [6.0, 7.0]]), "3"),
                    (4, numpy.array([[5.0, 1.0], [8.0, 3.0], [2.0, 9.0]]), "6"),
                ],
                dtype=[("a", "i8"), ("b", "(3,2)f4"), ("c", "U5")],
            ),
            serialized={
                "dtype": None,
                "shape": (2,),
                "array": None,
                "fields": {
                    "a": {
                        "dtype": str(numpy.dtype("i8")),
                        "shape": (2,),
                        "array": [1, 4],
                        "fields": None,
                    },
                    "b": {
                        "dtype": str(numpy.dtype("f4")),
                        "shape": (2, 3, 2),
                        "array": [
                            [[2.0, 4.0], [3.0, 9.0], [6.0, 7.0]],
                            [[5.0, 1.0], [8.0, 3.0], [2.0, 9.0]],
                        ],
                        "fields": None,
                    },
                    "c": {
                        "dtype": str(numpy.dtype("U5")),
                        "shape": (2,),
                        "array": ["3", "6"],
                        "fields": None,
                    },
                },
            },
        ),
    }
    array_field = fields.NumpyArray()

    @pytest.fixture(
        scope="class", params=tested_arrays.values(), ids=tested_arrays.keys()
    )
    def an_array(self, request) -> Fixture:
        """Returns different arrays (declared in the :attr:`tested_arrays` class attribute) for testing.
        """
        return request.param

    @pytest.fixture(scope="class")
    def deserialize(self) -> Callable[[dict], numpy.ndarray]:
        """Shortcut function that deserializes a :class:`numpy.ndarray`.
        """
        return lambda obj: self.array_field.deserialize(obj)

    @pytest.fixture(scope="class")
    def serialize(self) -> Callable[[numpy.ndarray], dict]:
        """Shortcut function that serializes a :class:`numpy.ndarray`.
        """
        return lambda array: self.array_field.serialize("value", {"value": array})

    def test_METHOD_deserialize(self, an_array, deserialize):
        # Given the fixtures.

        # When
        deserialized_array = deserialize(an_array.serialized)

        # Then
        assert numpy.all(deserialized_array == an_array.numpy_array)

    def test_METHOD_serialize(self, an_array, serialize):
        # Given the fixtures.

        # When
        serialized_array = serialize(an_array.numpy_array)

        # Then
        assert serialized_array == an_array.serialized

    def test_deserialize_and_serialize_methods_are_co_inverses(
        self, an_array, deserialize, serialize
    ):
        # Given the fixtures.

        # When
        de_se = deserialize(serialize(an_array.numpy_array))
        se_de = serialize(deserialize(an_array.serialized))

        # Then
        assert numpy.all(de_se == an_array.numpy_array)
        assert se_de == an_array.serialized


class Test_PhysicalQuantity_without_unit_validation:
    """Test :class:`v2.server.rest.fields.PhysicalQuantity` field specifiers created **without** an explicit compatible unit
    type.
    """

    @attr.s
    class FixtureBase:
        as_string = attr.ib(type=bool, kw_only=True)
        magnitude_type = attr.ib(type=fields.Field, default=None, kw_only=True)

        @property
        def quantity_field(self):
            return fields.PhysicalQuantity(
                magnitude_type=self.magnitude_type, as_string=self.as_string
            )

        def deserialize(self, obj: str) -> Quantity:
            return self.quantity_field.deserialize(obj)

        def serialize(self, obj: Quantity) -> str:
            return self.quantity_field.serialize("val", {"val": obj})

    @attr.s
    class InvalidQuantityFixture(FixtureBase):
        serialized = attr.ib(type=str, kw_only=True)

    @attr.s
    class ValidQuantityFixture(FixtureBase):
        quantity = attr.ib(type=Quantity, kw_only=True)
        serialized = attr.ib(type=str, kw_only=True)

    valid_quantities = {
        **{
            f'23*hertz-{"str" if as_string else "dict"}': dict(
                as_string=as_string,
                quantity=23 * units.hertz,
                serialized={"magnitude": 23, "unit": "hertz"}
                if not as_string
                else str(23 * units.hertz),
            )
            for as_string in [False, True]
        },
        **{
            f'45.6*second-{"str" if as_string else "dict"}': dict(
                as_string=as_string,
                quantity=45.6 * units.second,
                serialized={"magnitude": 45.6, "unit": "second"}
                if not as_string
                else str(45.6 * units.second),
            )
            for as_string in [False, True]
        },
        **{
            f'[12,34]*meter/second-{"str" if as_string else "dict"}': dict(
                as_string=as_string,
                magnitude_type=fields.NumpyArray,
                quantity=numpy.array([12.0, 34.0]) * units.meter / units.second,
                serialized={
                    "magnitude": {
                        "dtype": str(numpy.dtype("f8")),
                        "shape": (2,),
                        "array": [12.0, 34.0],
                        "fields": None,
                    },
                    "unit": str(units.meter / units.second),
                }
                if not as_string
                else str(numpy.array([12.0, 34.0]) * units.hertz),
            )
            # TODO: The current version of Pint is able to stringify array magnitudes, but not to parse them back.
            for as_string in [False]
        },
    }

    invalid_quantities = {
        **{
            f'hertzQW-{"str" if as_string else "dict"}': dict(
                as_string=as_string,
                serialized={"magnitude": 23, "unit": "hertzQW"}
                if not as_string
                else "23 hertzQW",
            )
            for as_string in [False, True]
        },
        **{
            f'secondISH-{"str" if as_string else "dict"}': dict(
                as_string=as_string,
                serialized={"magnitude": 45.6, "unit": "secondISH"}
                if not as_string
                else "45.6 secondISH",
            )
            for as_string in [False, True]
        },
    }

    @pytest.fixture(
        scope="class", params=valid_quantities.values(), ids=valid_quantities.keys()
    )
    def valid_quantity(self, request) -> ValidQuantityFixture:
        return self.ValidQuantityFixture(**request.param)

    @pytest.fixture(
        scope="class", params=invalid_quantities.values(), ids=invalid_quantities.keys()
    )
    def invalid_quantity(self, request) -> InvalidQuantityFixture:
        return self.InvalidQuantityFixture(**request.param)

    def test_METHOD_deserialize_RAISES_ValidationError_WITH_invalid_quantity(
        self, invalid_quantity
    ):
        # Given
        deserialize = invalid_quantity.deserialize

        # When, then
        with pytest.raises(ValidationError):
            deserialize(invalid_quantity.serialized)

    def test_METHOD_deserialize_WITH_valid_quantity(self, valid_quantity):
        # Given
        deserialize = valid_quantity.deserialize

        # When
        deserialized_quantity = deserialize(valid_quantity.serialized)

        # Then
        assert numpy.all(deserialized_quantity == valid_quantity.quantity)

    def test_METHOD_serialize_WITH_valid_quantity(self, valid_quantity):
        # Given
        serialize = valid_quantity.serialize

        # When
        serialized_quantity = serialize(valid_quantity.quantity)

        # Then
        assert serialized_quantity == valid_quantity.serialized

    def test_deserialize_and_serialize_methods_are_co_inverses_WITH_valid_quantity(
        self, valid_quantity
    ):
        # Given.
        deserialize = valid_quantity.deserialize
        serialize = valid_quantity.serialize

        # When
        se_de = serialize(deserialize(valid_quantity.serialized))
        de_se = deserialize(serialize(valid_quantity.quantity))

        # Then
        assert se_de == valid_quantity.serialized
        assert numpy.all(de_se == valid_quantity.quantity)


class Test_PhysicalQuantity_WITH_unit_validation:
    """Test :class:`v2.server.rest.fields.PhysicalQuantity` field specifiers created **with** an explicit compatible unit
    type.
    """

    @attr.s
    class CheckedQuantityFixture:
        as_string = attr.ib(type=bool)
        compatible_unit = attr.ib(type=Unit)

        # Notice that, when an incompatible unit is set, the fields below (object form and serialized form) do not need to
        # be consistent with each other, because test cases using such fixture will typically only use one of these values,
        # check for an exception during (de)serialization (since the compatible_unit field actually contains an incompatible
        # unit) then, cause a test failure to be reported.
        quantity = attr.ib(type=Quantity)
        serialized = attr.ib(type=str)

        @property
        def field(self) -> fields.PhysicalQuantity:
            return fields.PhysicalQuantity(
                unit=self.compatible_unit, as_string=self.as_string
            )

        def deserialize(self, obj: str) -> Quantity:
            return self.field.deserialize(obj)

        def serialize(self, obj: Quantity) -> str:
            return self.field.serialize("val", {"val": obj})

    compatible_quantities = {
        **{
            f'23*hertz-{unit!s}-{"str" if as_string else "dict"}': dict(
                as_string=as_string,
                compatible_unit=unit,
                quantity=23 * units.hertz,
                serialized={
                    "magnitude": (23 * units.hertz).to(unit).m,
                    "unit": str(unit),
                }
                if not as_string
                else str((23 * units.hertz).to(unit)),
            )
            for unit, as_string in product(
                [units.hertz, units.megahertz], [False, True]
            )
        },
        **{
            f'45.6*second-{unit!s}-{"str" if as_string else "dict"}': dict(
                as_string=as_string,
                compatible_unit=unit,
                quantity=45.6 * units.second,
                serialized={
                    "magnitude": (45.6 * units.second).to(unit).m,
                    "unit": str(unit),
                }
                if not as_string
                else str((45.6 * units.second).to(unit)),
            )
            for unit, as_string in product([units.second, units.hour], [False, True])
        },
    }

    incompatible_quantities = {
        **{
            f'23*hertz-{unit!s}-{"str" if as_string else "dict"}': dict(
                as_string=as_string,
                compatible_unit=unit,
                quantity=23 * units.hertz,
                serialized={"magnitude": 23, "unit": str(units.hertz)}
                if not as_string
                else str(23 * units.hertz),
            )
            for unit, as_string in product([units.second, units.meter], [False, True])
        },
        **{
            f'45.6*second-{unit!s}-{"str" if as_string else "dict"}': dict(
                as_string=as_string,
                compatible_unit=unit,
                quantity=45.6 * units.second,
                serialized={"magnitude": 45.6, "unit": str(units.second)}
                if not as_string
                else str(45.6 * units.second),
            )
            for unit, as_string in product(
                [units.kilobyte, units.ampere], [False, True]
            )
        },
    }

    @pytest.fixture(
        scope="class",
        params=compatible_quantities.values(),
        ids=compatible_quantities.keys(),
    )
    def compatible_unit(self, request) -> CheckedQuantityFixture:
        return self.CheckedQuantityFixture(**request.param)

    @pytest.fixture(
        scope="class",
        params=incompatible_quantities.values(),
        ids=incompatible_quantities.keys(),
    )
    def incompatible_unit(self, request) -> CheckedQuantityFixture:
        return self.CheckedQuantityFixture(**request.param)

    def test_METHOD_deserialize_RAISES_ValidationError_WITH_incompatible_unit(
        self, incompatible_unit
    ):
        # Given
        deserialize = incompatible_unit.deserialize
        obj = incompatible_unit.serialized

        # When, then
        with pytest.raises(ValidationError):
            deserialize(obj)

    def test_METHOD_deserialize_WITH_compatible_unit(self, compatible_unit):
        # Given
        deserialize = compatible_unit.deserialize

        # When
        deserialized_quantity = deserialize(compatible_unit.serialized)

        # Then
        assert deserialized_quantity == compatible_unit.quantity

    def test_METHOD_serialize_RAISES_ValidationError_WITH_incompatible_unit(
        self, incompatible_unit
    ):
        # Given
        serialize = incompatible_unit.serialize
        quantity = incompatible_unit.quantity

        with pytest.raises(ValidationError):
            serialize(quantity)

    def test_METHOD_serialize_WITH_compatible_unit(self, compatible_unit):
        # Given
        serialize = compatible_unit.serialize
        quantity = compatible_unit.quantity
        answer = compatible_unit.serialized

        # When
        result = serialize(quantity)

        # Then
        assert result == answer

    def test_deserialize_and_serialize_methods_are_co_inverses_WITH_compatible_unit(
        self, compatible_unit
    ):
        # Given.
        deserialize = compatible_unit.deserialize
        serialize = compatible_unit.serialize

        # When
        se_de = serialize(deserialize(compatible_unit.serialized))
        de_se = deserialize(serialize(compatible_unit.quantity))

        # Then
        assert se_de == compatible_unit.serialized
        assert de_se == compatible_unit.quantity


class Test_PhysicalUnit_without_unit_validation:
    """Test :class:`v2.server.rest.fields.PhysicalUnit` field specifiers created **without** an explicit compatible unit
    type.
    """

    class FixtureBase:
        unit_field = fields.PhysicalUnit()

        def deserialize(self, obj: str) -> Unit:
            return self.unit_field.deserialize(obj)

        def serialize(self, obj: Unit) -> str:
            return self.unit_field.serialize("val", {"val": obj})

    @attr.s
    class InvalidUnitFixture(FixtureBase):
        serialized = attr.ib(type=str)

    @attr.s
    class ValidUnitFixture(FixtureBase):
        unit = attr.ib(type=Unit)
        serialized = attr.ib(type=str)

    valid_units = {
        "hertz": dict(unit=units.hertz, serialized="hertz"),
        "second": dict(unit=units.second, serialized="second"),
    }

    invalid_units = {
        "hertzQW": dict(serialized="hertzQW"),
        "secondISH": dict(serialized="secondISH"),
    }

    @pytest.fixture(scope="class", params=valid_units.values(), ids=valid_units.keys())
    def valid_unit(self, request) -> ValidUnitFixture:
        return self.ValidUnitFixture(**request.param)

    @pytest.fixture(
        scope="class", params=invalid_units.values(), ids=invalid_units.keys()
    )
    def invalid_unit(self, request) -> InvalidUnitFixture:
        return self.InvalidUnitFixture(**request.param)

    def test_METHOD_deserialize_RAISES_ValidationError_WITH_invalid_unit(
        self, invalid_unit
    ):
        # Given
        deserialize = invalid_unit.deserialize

        # When, then
        with pytest.raises(ValidationError):
            deserialize(invalid_unit.serialized)

    def test_MEHTOD_deserialize_WITH_valid_unit(self, valid_unit):
        # Given
        deserialize = valid_unit.deserialize

        # When
        deserialized_unit = deserialize(valid_unit.unit)

        # Then
        assert deserialized_unit == valid_unit.unit

    def test_METHOD_serialize_WITH_valid_unit(self, valid_unit):
        # Given
        serialize = valid_unit.serialize

        # When
        serialized_unit = serialize(valid_unit.unit)

        # Then
        assert serialized_unit == valid_unit.serialized

    def test_deserialize_and_serialize_methods_are_co_inverses_WITH_valid_unit(
        self, valid_unit
    ):
        # Given.
        deserialize = valid_unit.deserialize
        serialize = valid_unit.serialize

        # When
        se_de = serialize(deserialize(valid_unit.serialized))
        de_se = deserialize(serialize(valid_unit.unit))

        # Then
        assert se_de == valid_unit.serialized
        assert de_se == valid_unit.unit


class Test_PhysicalUnit_WITH_unit_validation:
    """Test :class:`v2.server.rest.fields.PhysicalUnit` field specifiers created **with** an explicit compatible unit
    type.
    """

    @attr.s
    class CheckedUnitFixture:
        compatible_unit = attr.ib(type=Unit)
        unit = attr.ib(type=Unit)
        serialized = attr.ib(type=str)

        @property
        def field(self) -> fields.PhysicalUnit:
            return fields.PhysicalUnit(unit=self.compatible_unit)

        def deserialize(self, obj: str) -> Unit:
            return self.field.deserialize(obj)

        def serialize(self, obj: Unit) -> str:
            return self.field.serialize("val", {"val": obj})

    compatible_units = {
        **{
            f"hertz-{unit!s}": dict(
                compatible_unit=unit, unit=units.hertz, serialized=str(units.hertz)
            )
            for unit in [units.hertz, units.megahertz]
        },
        **{
            f"second-{unit!s}": dict(
                compatible_unit=unit, unit=units.second, serialized=str(units.second)
            )
            for unit in [units.second, units.hour]
        },
    }

    incompatible_units = {
        **{
            f"hertz-{unit!s}": dict(
                compatible_unit=unit, unit=units.hertz, serialized=str(units.hertz)
            )
            for unit in [units.second, units.meter]
        },
        **{
            f"second-{unit!s}": dict(
                compatible_unit=unit, unit=units.second, serialized=str(units.second)
            )
            for unit in [units.kilobyte, units.ampere]
        },
    }

    @pytest.fixture(
        scope="class", params=compatible_units.values(), ids=compatible_units.keys()
    )
    def compatible_unit(self, request) -> CheckedUnitFixture:
        return self.CheckedUnitFixture(**request.param)

    @pytest.fixture(
        scope="class", params=incompatible_units.values(), ids=incompatible_units.keys()
    )
    def incompatible_unit(self, request) -> CheckedUnitFixture:
        return self.CheckedUnitFixture(**request.param)

    def test_METHOD_deserialize_RAISES_ValidationError_WITH_incompatible_unit(
        self, incompatible_unit
    ):
        # Given
        deserialize = incompatible_unit.deserialize
        obj = incompatible_unit.serialized

        # When, then
        with pytest.raises(ValidationError):
            deserialize(obj)

    def test_METHOD_deserialize_WITH_compatible_unit(self, compatible_unit):
        # Given
        deserialize = compatible_unit.deserialize
        obj = compatible_unit.serialized
        answer = compatible_unit.unit

        # When
        deserialized_unit = deserialize(obj)

        # Then
        assert deserialized_unit == answer

    def test_MEHTOD_serialize_RAISES_ValidationError_WITH_incompatible_unit(
        self, incompatible_unit
    ):
        # Given
        serialize = incompatible_unit.serialize
        unit = incompatible_unit.unit

        with pytest.raises(ValidationError):
            serialize(unit)

    def test_METHOD_serialize_WITH_compatible_unit(self, compatible_unit):
        # Given
        serialize = compatible_unit.serialize
        unit = compatible_unit.unit
        answer = compatible_unit.serialized

        # When
        result = serialize(unit)

        # Then
        assert result == answer

    def test_deserialize_and_serialize_methods_are_co_inverses_WITH_compatible_unit(
        self, compatible_unit
    ):
        # Given.
        deserialize = compatible_unit.deserialize
        serialize = compatible_unit.serialize

        # When
        se_de = serialize(deserialize(compatible_unit.serialized))
        de_se = deserialize(serialize(compatible_unit.unit))

        # Then
        assert se_de == compatible_unit.serialized
        assert de_se == compatible_unit.unit
